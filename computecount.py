#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math


class Compute:

    def __init__(self, default, num1, num2, count):
        """Método iniciliazador"""
        self.default = default
        self.num1 = num1
        self.num2 = num2
        self.count = count

    def power(self):
        """ Calcula la potencia"""
        self.count = self.count + 1
        return float(self.num1) ** float(self.num2)

    def log(self):
        """ Calcula el logaritmo"""
        self.count = self.count + 1
        return math.log(float(self.num1), float(self.num2))

    def set_def(self):
        """ Utiliza el valor por defecto como base o exponente """
        if self.num2 == '':
            self.num2 = float(self.default)
            return self.num2

        elif float(self.num2) <= 0:
            raise ValueError('ValueError')
        else:
            return float(self.num2)

    def get_def(self):
        """ Devuelve el valor por defecto utilizado """
        return self.default
