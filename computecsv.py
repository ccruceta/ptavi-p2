#!/usr/bin/python3
# -*- coding: utf-8 -*-

import computecount
import sys


def process_csv(fich):

    datos = fich.read()
    """Leemos el fichero y comprobamos que el primer valor es el valor por defecto correcto"""
    try:
        default = float(datos.split('\n')[0])
    except:
        default = 2.0

    if default > 0:
        print(f"Default: {default}")
        count = 0
        for linea in (datos.split('\n')[1:]):
            """Leemos cada linea del fichero y sacamos cada uno de los operandos"""
            if linea != "":
                elementos = linea.split(',')
                operacion = elementos[0]
                num1 = elementos[1]
                if len(elementos) < 3:
                    num2 = ''
                else:
                    num2 = elementos[2]
                objeto = computecount.Compute(default, num1, num2, count)
                objeto.set_def()
                if len(elementos) <= 3:
                    """Llevamos a cabo la operaci´on oportuna en cada caso"""
                    if operacion == "power":
                        resultado = objeto.power()
                        count = count + 1

                    elif operacion == "log":
                        resultado = objeto.log()
                        count = count + 1

                    else:
                        print('Bad format')
                        continue
                    print(resultado)

                else:
                    print('Bad format')
                    continue
        print(f"Operations: {objeto.count}")
        fichero.close()
    else:
        raise ValueError("Error: default value must be greater than zero")
        sys.exit()


if __name__ == "__main__":
    """Abrimos el fichero en modo lectura y se lo pasamos a nuestra funci´on"""
    fichero = open(sys.argv[1], 'r')
    process_csv(fichero)
