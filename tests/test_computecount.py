#!/usr/bin/python3
# -*- coding: utf-8 -*-

import unittest
import computecount


class TestComputeCount(unittest.TestCase):

    def setUp(self):
        self.compute = computecount.Compute()

    def test_power(self):
        self.assertEqual(self.compute.power(2, 3), 8)
        self.assertEqual(self.compute.count, 1)

    def test_log(self):
        self.assertEqual(self.compute.log(8, 2), 3)
        self.assertEqual(self.compute.count, 1)

    def test_default_bad(self):
        with self.assertRaises(ValueError):
            self.compute.set_def(0)
        with self.assertRaises(ValueError):
            self.compute.set_def(-1)

    def test_default_power(self):
        self.compute.set_def(2)
        self.assertEqual(self.compute.power(2), 4)
        self.assertEqual(self.compute.log(4), 2)

    def test_count(self):
        self.compute.power(2, 3)
        self.compute.power(2, 4)
        self.compute.log(8, 2)
        self.compute.log(16, 2)
        self.assertEqual(self.compute.count, 4)


if __name__ == '__main__':
    unittest.main()
