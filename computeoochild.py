#!/usr/bin/python3
# -*- coding: utf-8 -*-

import computeoo
import sys


class ComputeChild(computeoo.Compute):

    def __init__(self, default, num1, num2):
        """ Método iniciliazador """
        self.default = default
        self.num1 = num1
        self.num2 = num2

    def set_def(self):
        """ Utiliza el valor por defecto como base o exponente """
        if self.num2 == '':
            self.num2 = float(self.default)
            return self.num2

        elif float(self.num2) <= 0:
            raise ValueError('ValueError')
        else:
            return float(self.num2)

    def get_def(self):
        """ Devuelve el valor por defecto utilizado """
        return self.default


if __name__ == "__main__":

    objeto = ComputeChild(2, sys.argv[2], sys.argv[3])

    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        objeto.num1 = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        objeto.num2 = 2
    else:
        try:
            objeto.num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        result = objeto.power()
    elif sys.argv[1] == "log":
        result = objeto.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)
