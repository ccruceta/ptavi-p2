#!/usr/bin/python3
# -*- coding: utf-8 -*-

import math
import sys


class Compute:

    def __init__(self, default, num1, num2):
        """Método iniciliazador"""
        self.default = default
        self.num1 = num1
        self.num2 = num2

    def power(self):
        """ Calcula la potencia"""
        return self.num1 ** self.num2

    def log(self):
        """ Calcula el logaritmo"""
        return math.log(self.num1, self.num2)


if __name__ == "__main__":

    objeto = Compute(2, sys.argv[2], sys.argv[3])

    if len(sys.argv) < 3:
        sys.exit("Error: at least two arguments are needed")

    try:
        objeto.num1 = float(sys.argv[2])
    except ValueError:
        sys.exit("Error: second argument should be a number")

    if len(sys.argv) == 3:
        objeto.num2 = 2
    else:
        try:
            objeto.num2 = float(sys.argv[3])
        except ValueError:
            sys.exit("Error: third argument should be a number")

    if sys.argv[1] == "power":
        result = objeto.power()
    elif sys.argv[1] == "log":
        result = objeto.log()
    else:
        sys.exit('Operand should be power or log')

    print(result)
